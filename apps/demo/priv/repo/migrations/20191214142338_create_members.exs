defmodule Demo.Repo.Migrations.CreateMembers do
  use Ecto.Migration

  def change do
    create table(:members) do
      add :first_name, :string
      add :middle_name, :string
      add :last_name, :string
      add :birthdate, :date

      timestamps()
    end

  end
end
