# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Demo.Repo.insert!(%Demo.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

n = 999

alias Faker.{Date, Name}

defmodule Seed do
  def fake_person() do
    first_name =
      case :rand.uniform(3) do
        1 -> Name.first_name() <> " " <> Name.first_name()
        _ -> Name.first_name()
      end

    %{
      first_name: first_name,
      middle_name: Name.last_name(),
      last_name: Name.last_name(),
      birthdate: Date.date_of_birth(80)
    }
  end
end

for i <- 1..n do
  {:ok, member} = Demo.Members.create_member(Seed.fake_person())
end
