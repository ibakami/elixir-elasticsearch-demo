defmodule Demo.MembersTest do
  use Demo.DataCase

  alias Demo.Members

  describe "members" do
    alias Demo.Members.Member

    @valid_attrs %{birthdate: ~D[2010-04-17], first_name: "some first_name", last_name: "some last_name", middle_name: "some middle_name"}
    @update_attrs %{birthdate: ~D[2011-05-18], first_name: "some updated first_name", last_name: "some updated last_name", middle_name: "some updated middle_name"}
    @invalid_attrs %{birthdate: nil, first_name: nil, last_name: nil, middle_name: nil}

    def member_fixture(attrs \\ %{}) do
      {:ok, member} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Members.create_member()

      member
    end

    test "list_members/0 returns all members" do
      member = member_fixture()
      assert Members.list_members() == [member]
    end

    test "get_member!/1 returns the member with given id" do
      member = member_fixture()
      assert Members.get_member!(member.id) == member
    end

    test "create_member/1 with valid data creates a member" do
      assert {:ok, %Member{} = member} = Members.create_member(@valid_attrs)
      assert member.birthdate == ~D[2010-04-17]
      assert member.first_name == "some first_name"
      assert member.last_name == "some last_name"
      assert member.middle_name == "some middle_name"
    end

    test "create_member/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Members.create_member(@invalid_attrs)
    end

    test "update_member/2 with valid data updates the member" do
      member = member_fixture()
      assert {:ok, %Member{} = member} = Members.update_member(member, @update_attrs)
      assert member.birthdate == ~D[2011-05-18]
      assert member.first_name == "some updated first_name"
      assert member.last_name == "some updated last_name"
      assert member.middle_name == "some updated middle_name"
    end

    test "update_member/2 with invalid data returns error changeset" do
      member = member_fixture()
      assert {:error, %Ecto.Changeset{}} = Members.update_member(member, @invalid_attrs)
      assert member == Members.get_member!(member.id)
    end

    test "delete_member/1 deletes the member" do
      member = member_fixture()
      assert {:ok, %Member{}} = Members.delete_member(member)
      assert_raise Ecto.NoResultsError, fn -> Members.get_member!(member.id) end
    end

    test "change_member/1 returns a member changeset" do
      member = member_fixture()
      assert %Ecto.Changeset{} = Members.change_member(member)
    end
  end
end
