defmodule Demo.Members do
  @moduledoc """
  The Members context.
  """

  import Ecto.Query, warn: false

  alias Demo.Members.Member
  alias Demo.Repo

  require Logger

  @doc """
  Returns the list of members.

  ## Examples

      iex> list_members()
      [%Member{}, ...]

  """
  def list_members do
    Repo.all(Member)
  end

  @doc """
  Gets a single member.

  Raises `Ecto.NoResultsError` if the Member does not exist.

  ## Examples

      iex> get_member!(123)
      %Member{}

      iex> get_member!(456)
      ** (Ecto.NoResultsError)

  """
  def get_member!(id), do: Repo.get!(Member, id)

  @doc """
  Creates a member.

  ## Examples

      iex> create_member(%{field: value})
      {:ok, %Member{}}

      iex> create_member(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_member(attrs \\ %{}) do
    %Member{}
    |> Member.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a member.

  ## Examples

      iex> update_member(member, %{field: new_value})
      {:ok, %Member{}}

      iex> update_member(member, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_member(%Member{} = member, attrs) do
    member
    |> Member.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Member.

  ## Examples

      iex> delete_member(member)
      {:ok, %Member{}}

      iex> delete_member(member)
      {:error, %Ecto.Changeset{}}

  """
  def delete_member(%Member{} = member) do
    Repo.delete(member)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking member changes.

  ## Examples

      iex> change_member(member)
      %Ecto.Changeset{source: %Member{}}

  """
  def change_member(%Member{} = member) do
    Member.changeset(member, %{})
  end

  @doc """
  Search for a member using ILIKE
  """
  def search_ilike(s, offset \\ 0, limit \\ 100) do
    terms = s |> String.split() |> Enum.map(&"%#{String.trim(&1)}%")

    query =
      Enum.reduce(terms, Member, fn pattern, query ->
        from m in query,
          or_where: ilike(m.first_name, ^pattern),
          or_where: ilike(m.middle_name, ^pattern),
          or_where: ilike(m.last_name, ^pattern)
      end)

    Repo.all(from q in query, offset: ^offset, limit: ^limit)
  end

  @name_fields [:first_name, :middle_name, :last_name]
  @search_path "/members/_search"

  @doc """
  Search for a member Elasticsearch "prefix" query
  """
  def search_prefix(s, from \\ 0, size \\ 100) do
    terms = s |> String.split() |> Enum.map(&String.trim/1)

    should =
      Enum.reduce(terms, [], fn pattern, should ->
        Enum.reduce(@name_fields, should, fn field, should ->
          [%{prefix: %{field => pattern}} | should]
        end)
      end)

    query = %{
      query: %{
        bool: %{
          should: should,
          minimum_should_match: 1
        }
      },
      from: from,
      size: size
    }

    Logger.debug("query => #{inspect(query)}")

    Demo.Elasticsearch
    |> Elasticsearch.post(@search_path, query)
  end

  @doc """
  Search for a member Elasticsearch "wildcard" query
  """
  def search_wildcard(s, from \\ 0, size \\ 100) do
    terms = s |> String.split() |> Enum.map(&String.trim/1)

    should =
      Enum.reduce(terms, [], fn pattern, should ->
        Enum.reduce(@name_fields, should, fn field, should ->
          [%{wildcard: %{field => "*" <> pattern <> "*"}} | should]
        end)
      end)

    query = %{
      query: %{
        bool: %{
          should: should,
          minimum_should_match: 1
        }
      },
      from: from,
      size: size
    }

    Logger.debug("query => #{inspect(query)}")

    Demo.Elasticsearch
    |> Elasticsearch.post(@search_path, query)
  end

  @doc """
  Search for a member Elasticsearch "query_string" query
  """
  def search_query_string(s, from \\ 0, size \\ 100) do
    terms = s |> String.split() |> Enum.map(&("*" <> String.trim(&1) <> "*")) |> Enum.join(" ")

    query = %{
      query: %{
        query_string: %{
          query: terms,
          fields: @name_fields
        }
      },
      from: from,
      size: size
    }

    Logger.debug("query => #{inspect(query)}")

    Demo.Elasticsearch |> Elasticsearch.post(@search_path, query)
  end
end
