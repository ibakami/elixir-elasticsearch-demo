defmodule Demo.Members.Member do
  use Ecto.Schema

  alias __MODULE__

  import Ecto.Changeset

  schema "members" do
    field :first_name, :string
    field :middle_name, :string
    field :last_name, :string
    field :birthdate, :date

    timestamps()
  end

  @spec display_name(%Member{}) :: String.t()
  def display_name(member) do
    [member.first_name, member.middle_name, member.last_name]
    |> Enum.reject(&is_nil/1)
    |> Enum.join(" ")
  end

  @doc false
  def changeset(member, attrs) do
    member
    |> cast(attrs, [:first_name, :middle_name, :last_name, :birthdate])
    |> validate_required([:first_name, :last_name, :birthdate])
  end
end

defimpl Elasticsearch.Document, for: Demo.Members.Member do
  def id(member), do: member.id
  def routing(_), do: false

  def encode(member) do
    member |> Map.from_struct() |> Map.take([:first_name, :middle_name, :last_name, :birthdate])
  end
end
