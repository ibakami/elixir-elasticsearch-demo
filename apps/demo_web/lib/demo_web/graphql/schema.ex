defmodule DemoWeb.GraphQL.Schema do
  @moduledoc """
  Defines the root query and mutations
  """

  use Absinthe.Schema
  import_types(Absinthe.Type.Custom)

  query do
    field :version, :string do
      resolve(fn _, _, _ ->
        {:ok, Application.spec(:shore_api, :vsn)}
      end)
    end
  end

  mutation do
  end
end
