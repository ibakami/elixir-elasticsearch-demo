defmodule DemoWeb.Router do
  use DemoWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", DemoWeb do
    pipe_through :browser

    get "/", PageController, :index
  end

  scope "/" do
    pipe_through(:api)

    forward("/graphql", Absinthe.Plug, schema: DemoWeb.GraphQL.Schema, json_codec: Jason)

    forward("/graphiql", Absinthe.Plug.GraphiQL,
      schema: DemoWeb.GraphQL.Schema,
      context: %{pubsub: DemoWeb.Endpoint},
      json_codec: Jason
    )
  end

  scope "/api" do
    pipe_through(:api)
    resources("/members", DemoWeb.MemberController)
  end

  # Other scopes may use custom stacks.
  # scope "/api", DemoWeb do
  #   pipe_through :api
  # end
end
