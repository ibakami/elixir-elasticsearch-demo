require Logger

Benchee.run(
  %{
    "ilike" => fn s ->
      members = Demo.Members.search_ilike(s)
      Logger.debug("Got #{Enum.count(members)} hits")
    end,
    "prefix" => fn s ->
      {:ok, results} = Demo.Members.search_prefix(s)
      Logger.debug("Got #{Enum.count(results["hits"]["hits"])} hits")
    end,
    "wildcard" => fn s ->
      {:ok, results} = Demo.Members.search_wildcard(s)
      Logger.debug("Got #{Enum.count(results["hits"]["hits"])} hits")
    end,
    "query_string" => fn s ->
      {:ok, results} = Demo.Members.search_query_string(s)
      Logger.debug("Got #{Enum.count(results["hits"]["hits"])} hits")
    end
  },
  time: 30,
  inputs: %{
    ~s["alistair"] => "alistair",
    ~s["isr"] => "isr",
    ~s["ali isr"] => "ali isr"
  },
  formatters: [
    Benchee.Formatters.HTML,
    Benchee.Formatters.Console
  ]
)
